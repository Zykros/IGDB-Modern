# IGDB Widget
=== Plugin Name ===
Contributors: Eddie Stubbington
Tags: gaming,releases,igdb
Requires at least: 3.0.1
Tested up to: 5.1.0
Stable tag: 5.1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A widget for IGDB's Upcoming Games Lists. To use the widget you need an API Key from the API.IGDB.com website. 

== Description ==

This plugin is used to display a list of upcoming game releases. It uses the IGDB.com API and has a variety of options to customise the way the list is displayed. The plugins aim is to remove the manual effort that most gaming media websites have when displaying releases. 

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Use the Appearance > Customise > Widgets area to find and customise the plugin 
4. Go to API.igbd.com
5. Sign in/create an account
6. Select the Key 
7. Copy the API key
8. Paste API key into the API Field on the widgets settings page
9. Save
10. Refresh to activate the API
11. Your setup and ready to go

== Frequently Asked Questions ==

= Is the API Free? =

Yes, but you need to register on the API.IGDB.com website. Some sites may need to pay per month to use the API depending on the number of requests your site makes. More info can be found here - https://api.igdb.com/pricing

= Why are features missing  =

Multiple features have been disabled in this release, as they are not in fully working order but we wanted to get an update out to try and fix the major issues. They will be re-enabled in due course. If your plugin doesn't work / a feature is missing please view https://gitlab.com/edstub207/IGDB-Modern/issues for more details. You might need to re-install the plugin if one of the old settings is currently active behind the scenes. 


= How regular will updates be? =

Updates to the plugin will be as frequent as possible. We are always adding new features and fixing bugs. 

= I have found a bug =

Damn! Sorry about that. If you have a bug report it on https://gitlab.com/edstub207/IGDB-Modern/issues

= I want to help =

Awesome! Feel free to submit changes on the gitlab listed above :) 

= Why is the limit 50 games at once displayed =

We have recently increased this to 50 (Still 10 by standard). However, be warned this can result in a high number of requests, so could increase API costs. If you require more we can up the limit higher by request. However, we feel 50 is the max amount anyone would want to display. 

= Why are images missing =

Typically, this is caused by the IGDB Database not having any images suitable for the plugin. If you see no images at all though please do contact us. 

= I want XYZ to be an option with the plugin =

We don't run IGDB as a website! So we are slightly limited in what we can do in terms of API Calls etc. However, we can send feedback to the IGDB Team if needed. We can add features to the plugin if we have the API call. 


== Changelog ==
==2.5.0.1==
Fixing typo which broke plugin. 

==2.5.0.0==
Fixed bugs with data not being setup correctly on latest API Version. 
Increased the limit of games displayed to 50 from 10, default has been unchanged. 
Disabled the filter by franchise option as it wasn't due to be released. 
Disabled multiple broken features while fixes are in progress. 

==2.4.2.2==
Bug Fix which outputted extra headers which asserted out activation 
==2.4.2.1==
Bug fix for missing images. 
==2.4.2==
Added new "Hide Release Date" field 

==2.4.1.1==
Various bug fixes. 

==2.4.1==
Added new "Hide Counter" field on the configuration screen
Fixed multiple other bugs 

==2.4.0==
The IGDB API has been re-located to a new API Server. This update includes minor bug fixes and the changes required to port the plugin to the new API System.

= 2.3.0.1 =
Major Redesign 

== Upgrade Notice ==
==2.5.0.0==
Fixed bugs with data not being setup correctly on latest API Version. Required upgrade to use the plugin. Some features are known to be broken in this build and have been disabled. If you have issues please re-install or visit the website. 

==2.4.2.2==
Major Bug Fix. Please upgrade. 

==2.4.2==
Added new "Hide Release Date" field 
==2.4.1.1==
Various bug fixes.
=2.4.1=
Added new "Hide Counter" field on the configuration screen
Fixed multiple other bugs 

=2.4.0=
The IGDB API has been re-located to a new API Server. This update includes minor bug fixes and the changes required to port the plugin to the new API System.


= 2.3.0.1 = 
Major Redesign, please upgrade to have a "Modern look"
